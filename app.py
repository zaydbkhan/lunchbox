from flask import Flask, request
from waitress import serve

import uuid
import os
import re
import datetime as dt

import domain.user_service
import domain.school_service
from models.school import School
from models.user import User

app = Flask(__name__)


@app.after_request
def add_hostname_header(response):
    env_host = str(os.environ.get('HOSTNAME'))
    hostname = re.findall('[a-z]{3}-\d$', env_host)
    if hostname:
            response.headers["SP-LOCATION"] = hostname
    response.headers["MY_HEADER"] = "hello world"
    return response


@app.route('/')
def get_uuid():
    return str(uuid.uuid4())

@app.route('/getUser', methods = ['GET'])
def getUser():
    user_svc = domain.user_service.UserService()
    user_model = user_svc.get(request.args.get('username'))
    if (user_model != None):
        return user_model.to_json()
    else: 
        return "User not found"

@app.route('/addUser', methods = ['POST'])
def addUser():
    user_svc = domain.user_service.UserService()
    content_type = request.headers.get('Content-Type')
    if (content_type == 'application/json'):
        user_model = User.from_json(request.json)
        user_svc.add(user_model)
        return "posted"
    else:
        return 'content type not supported'

@app.route('/getSchool', methods = ['GET'])
def getSchool():
    school_svc = domain.school_service.SchoolService()
    school_model = school_svc.get(request.args.get('name'))
    if (school_model != None):
        return school_model.to_json()
    else: 
        return "User not found"

@app.route('/addSchool', methods = ['POST'])
def addSchool():
    school_svc = domain.school_service.SchoolService()
    content_type = request.headers.get('Content-Type')
    if (content_type == 'application/json'):
        school_model = School.from_json(request.json)
        school_svc.add(school_model)
        return "posted"
    else:
        return 'content type not supported'

if __name__ == "__main__":
    serve(app, listen='*:80')