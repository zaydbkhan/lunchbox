from peewee import *
from domain.user.user_entity import UserEntity
from domain.school.school_entity import SchoolEntity
from domain.db import get_db

def create_tables():
    with get_db().connection_context():
        get_db().create_tables([UserEntity, SchoolEntity])