from peewee import *
import os
from dotenv import load_dotenv

load_dotenv()

_database = MySQLDatabase(
    host = '127.0.0.1',
    user = 'root',
    password = os.getenv('DB_PASSWORD'),
    database = 'lunchbox'
)

def get_db():
    return _database