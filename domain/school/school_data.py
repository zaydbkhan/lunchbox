from pymysql import NULL
from domain.school.school_entity import SchoolEntity
from domain.db import get_db

class SchoolData():

    def __init__(self) -> None:
        pass

    def add(self, school_entity):
        with get_db().connection_context():
            school_entity.save()

    def get(self, name):
        with get_db().connection_context():
            school_entity = SchoolEntity.select().where(SchoolEntity.name == name)[0]
            if (school_entity == NULL):
                return None
            else:
                return school_entity