from peewee import *
import domain.db

class SchoolEntity(Model):

    name = CharField()
    district = CharField()
    address = CharField(max_length=1000)
    num_students_need_lunch = IntegerField()
    user_administrator = IntegerField()
    create_dt = DateField()
    update_dt = DateField()

    class Meta:
        database = domain.db.get_db()
        db_table = "School"