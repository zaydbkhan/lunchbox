from domain.school.school_data import SchoolData
from domain.school.school_entity import SchoolEntity
from models.school import School

class SchoolManager():

    def __init__(self):
        self._school_data = SchoolData()
    
    def add(self, school_model):
        school_entity = SchoolEntity.create(
            name = school_model.name,
            district = school_model.district,
            address = school_model.address,
            num_students_need_lunch = school_model.num_students_need_lunch,
            user_administrator = school_model.user_administrator,
            create_dt = school_model.create_dt,
            update_dt = school_model.update_dt
        )
        self._school_data.add(school_entity)

    def get(self, name):
        school_entity = self._school_data.get(name)

        if (school_entity == None):
            return None

        school_model = School(
            name = school_entity.name,
            district = school_entity.district,
            address = school_entity.address,
            num_students_need_lunch = school_entity.num_students_need_lunch,
            user_administrator = school_entity.user_administrator,
            create_dt = school_entity.create_dt,
            update_dt = school_entity.update_dt
        )

        return school_model