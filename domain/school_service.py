from domain.school.school_manager import SchoolManager

class SchoolService:

    def __init__(self):
        self._school_manager = SchoolManager()

    def add(self, user_model):
        self._school_manager.add(user_model)

    def get(self, name):
        return self._school_manager.get(name)