from pymysql import NULL
from domain.user.user_entity import UserEntity
from domain.db import get_db

class UserData():
    
    def __init__(self) -> None:
        pass

    def add(self, user_entity):
        with get_db().connection_context():
            user_entity.save()

    def get(self, username):
        with get_db().connection_context():
            user_entity = UserEntity.select().where(UserEntity.username == username)[0]
            if (user_entity == NULL):
                return None
            else:
                return user_entity