from peewee import *
import domain.db

class UserEntity(Model):

    username = CharField(unique=True, index=True)
    password = CharField()
    email = CharField(unique=True, index=True)
    administrator = BooleanField()
    school = IntegerField(null=True)
    create_dt = DateField()
    update_dt = DateField()

    class Meta:
        database = domain.db.get_db()
        db_table = "User"