from domain.user.user_data import UserData
from domain.user.user_entity import UserEntity
from models.user import User

class UserManager():

    def __init__(self):
        self._user_data = UserData()

    def add(self, user_model):
        user_entity = UserEntity(
            username = user_model.username,
            password = user_model.password,
            email = user_model.email,
            administrator = user_model.administrator,
            school = user_model.school,
            create_dt = user_model.create_dt,
            update_dt = user_model.update_dt
        )   
        self._user_data.add(user_entity)

    def get(self, username):
        user_entity = self._user_data.get(username)

        if (user_entity == None):
            return None

        user_model = User(
            username = user_entity.username,
            password = user_entity.password,
            email = user_entity.email,
            administrator = user_entity.administrator,
            school = user_entity.school,
            create_dt = user_entity.create_dt,
            update_dt = user_entity.update_dt
        )

        return user_model
