from domain.user.user_manager import UserManager

class UserService:

    def __init__(self):
        self._user_manager = UserManager()

    def add(self, user_model):
        self._user_manager.add(user_model)

    def get(self, username):
        return self._user_manager.get(username)