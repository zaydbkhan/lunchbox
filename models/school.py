import json
import datetime as dt

class School():

    def __init__(self, name, district, address, num_students_need_lunch, user_administrator, create_dt, update_dt): 
        self._name = name
        self._district = district
        self._address = address
        self._num_students_need_lunch = num_students_need_lunch
        self._user_administrator = user_administrator
        self._create_dt = create_dt
        self._update_dt = update_dt

    @staticmethod
    def from_json(school_json):
        now = dt.datetime.utcnow()
        school_model = School(
            name = school_json['name'],
            district = school_json['district'],
            address = school_json['address'],
            num_students_need_lunch = school_json['num_students_need_lunch'],
            user_administrator = school_json['user_administrator'],
            create_dt = now,
            update_dt = now
        )
        return school_model

    @property
    def name(self):
        return self._name

    @property
    def district(self):
        return self._district

    @property
    def address(self):
        return self._address

    @property
    def num_students_need_lunch(self):
        return self._num_students_need_lunch
    
    @property
    def user_administrator(self):
        return self._user_administrator
    
    @property
    def create_dt(self):
        return self._create_dt

    @property
    def update_dt(self):
        return self._update_dt

    def to_json(self):
        school_dict = {
            "name": self._name,
            "district": self._district,
            "address": self._address,
            "num_students_need_lunch": self._num_students_need_lunch,
            "user_administrator": self._user_administrator,
            "create_dt": str(self._create_dt),
            "update_dt": str(self._update_dt)
        }
        return json.dumps(school_dict)