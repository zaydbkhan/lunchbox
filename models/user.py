import json
import datetime as dt

class User():

    def __init__(self, username, password, email, administrator, school, create_dt, update_dt):
        self._username = username
        self._password = password
        self._email = email
        self._administrator = administrator
        self._school = school
        self._create_dt = create_dt
        self._update_dt = update_dt

    @staticmethod
    def from_json(user_json):
        now = dt.datetime.utcnow()
        user_model = User(
            username = user_json['username'],
            password = user_json['password'],
            email = user_json['email'],
            administrator = bool(user_json['administrator']),
            school = user_json['school'],
            create_dt = now,
            update_dt = now
        )
        return user_model

    @property
    def username(self):
        return self._username

    @property
    def password(self):
        return self._password

    @property
    def email(self):
        return self._email

    @property
    def administrator(self):
        return self._administrator

    @property
    def school(self):
        return self._school

    @property
    def create_dt(self):
        return self._create_dt
    
    @property
    def update_dt(self):
        return self._update_dt

    def to_json(self):
        user_dict = {
            "username": self._username,
            "password": self._password,
            "email": self._email,
            "administrator": self._administrator,
            "school": self._school,
            "create_dt": str(self._create_dt),
            "update_dt": str(self._update_dt)
        }
        return json.dumps(user_dict)